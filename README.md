# Presentation Utils

## notebook
The Jupyter notebook will create the animation file. Let it run and afterwards you will find the `animation.mp4` file in your working directory.
## convert_mp4_2_png
This script will convert the mp4 file into an image sequence. It expects 2 paramenters:

input mp4 file
framerate

**Example:**
./convert_mp4_2_png.sh animation.mp4 24

## convert_mp4_2_gif.sh
Will output a gif with a framerate of 10 fps

##Dependencies
The Pyswarm documentation recommends installing ffmpeg via
`conda install -c conda-forge ffmpeg`

Further dependencies are listed in `requirements.txt`
