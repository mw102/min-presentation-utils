#!/bin/bash

ffmpeg -i $1 -filter_complex 'fps=10,scale=512:-1:flags=lanczos,split [o1] [o2];[o1] palettegen [p]; [o2] fifo [o3];[o3] [p] paletteuse' out.gif
